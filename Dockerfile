FROM maven:3.9.9-eclipse-temurin-21

LABEL version=8.0
LABEL organization="MIAGE de Toulouse"
LABEL author="Cedric Teyssie"
LABEL org.opencontainers.image.authors="cedric.teyssie@miage.fr"

WORKDIR /app
COPY . .

RUN mvn clean package
CMD ["mvn", "spring-boot:run"]